/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.prueba1aplicaciones.modelo;

/**
 *
 * @author andre
 */
public class Calculadora {
    private int montoInversion;
    private int anhos;
    private int interes;
    private double Calculo;
    
    
    public Calculadora (String montoInversion, String anhos, String interes){
        this.montoInversion=Integer.parseInt(montoInversion);
        this.anhos=Integer.parseInt(anhos);
        this.interes=Integer.parseInt(interes);
    
    }

    public double getCalculo() {
        this.Calculo=this.montoInversion+this.montoInversion*this.anhos*this.interes/100;
        return Calculo;
    }

    public void setCalculo(double Calculo) {
        this.Calculo = Calculo;
    }



    public int getMontoInversion() {
        return montoInversion;
    }

    public void setMontoInversion(int montoInversion) {
        this.montoInversion = montoInversion;
    }

    public int getAnhos() {
        return anhos;
    }

    public void setAnhos(int anhos) {
        this.anhos = anhos;
    }

    public int getInteres() {
        return interes;
    }

    public void setInteres(int interes) {
        this.interes = interes;
    }


}
