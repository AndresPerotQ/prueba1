<%-- 
    Document   : resultado
    Created on : 28-09-2021, 17:52:15
    Author     : andre
--%>

<%@page import="ciisa.prueba1aplicaciones.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 

    Calculadora calc=(Calculadora)request.getAttribute("Calculo");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Para un capital de <%=calc.getMontoInversion()%> en un periodo <%=calc.getAnhos()%> años con un interes anual de <%=calc.getInteres()%>.</h1>
        <h1>El capital al final del periodo sera: <%= calc.getCalculo() %></h1>
        
    </body>
</html>
